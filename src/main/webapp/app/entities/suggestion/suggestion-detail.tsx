import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './suggestion.reducer';
import { ISuggestion } from 'app/shared/model/suggestion.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISuggestionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SuggestionDetail = (props: ISuggestionDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { suggestionEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Suggestion [<b>{suggestionEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">Name</span>
          </dt>
          <dd>{suggestionEntity.name}</dd>
          <dt>
            <span id="ascii">Ascii</span>
          </dt>
          <dd>{suggestionEntity.ascii}</dd>
          <dt>
            <span id="altName">Alt Name</span>
          </dt>
          <dd>{suggestionEntity.altName}</dd>
          <dt>
            <span id="featClass">Feat Class</span>
          </dt>
          <dd>{suggestionEntity.featClass}</dd>
          <dt>
            <span id="featCode">Feat Code</span>
          </dt>
          <dd>{suggestionEntity.featCode}</dd>
          <dt>
            <span id="country">Country</span>
          </dt>
          <dd>{suggestionEntity.country}</dd>
          <dt>
            <span id="cc2">Cc 2</span>
          </dt>
          <dd>{suggestionEntity.cc2}</dd>
          <dt>
            <span id="admin1">Admin 1</span>
          </dt>
          <dd>{suggestionEntity.admin1}</dd>
          <dt>
            <span id="admin2">Admin 2</span>
          </dt>
          <dd>{suggestionEntity.admin2}</dd>
          <dt>
            <span id="admin3">Admin 3</span>
          </dt>
          <dd>{suggestionEntity.admin3}</dd>
          <dt>
            <span id="admin4">Admin 4</span>
          </dt>
          <dd>{suggestionEntity.admin4}</dd>
          <dt>
            <span id="population">Population</span>
          </dt>
          <dd>{suggestionEntity.population}</dd>
          <dt>
            <span id="elevation">Elevation</span>
          </dt>
          <dd>{suggestionEntity.elevation}</dd>
          <dt>
            <span id="dem">Dem</span>
          </dt>
          <dd>{suggestionEntity.dem}</dd>
          <dt>
            <span id="tz">Tz</span>
          </dt>
          <dd>{suggestionEntity.tz}</dd>
          <dt>
            <span id="latitude">Latitude</span>
          </dt>
          <dd>{suggestionEntity.latitude}</dd>
          <dt>
            <span id="longitude">Longitude</span>
          </dt>
          <dd>{suggestionEntity.longitude}</dd>
        </dl>
        <Button tag={Link} to="/suggestion" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/suggestion/${suggestionEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ suggestion }: IRootState) => ({
  suggestionEntity: suggestion.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SuggestionDetail);
