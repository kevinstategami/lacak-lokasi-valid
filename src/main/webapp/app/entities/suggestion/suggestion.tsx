import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './suggestion.reducer';
import { ISuggestion } from 'app/shared/model/suggestion.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface ISuggestionProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Suggestion = (props: ISuggestionProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const { suggestionList, match, loading, totalItems } = props;
  return (
    <div>
      <h2 id="suggestion-heading">
        Suggestions
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Suggestion
        </Link>
      </h2>
      <div className="table-responsive">
        {suggestionList && suggestionList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('name')}>
                  Name <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ascii')}>
                  Ascii <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('altName')}>
                  Alt Name <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('featClass')}>
                  Feat Class <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('featCode')}>
                  Feat Code <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('country')}>
                  Country <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('cc2')}>
                  Cc 2 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('admin1')}>
                  Admin 1 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('admin2')}>
                  Admin 2 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('admin3')}>
                  Admin 3 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('admin4')}>
                  Admin 4 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('population')}>
                  Population <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('elevation')}>
                  Elevation <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('dem')}>
                  Dem <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('tz')}>
                  Tz <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('latitude')}>
                  Latitude <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('longitude')}>
                  Longitude <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {suggestionList.map((suggestion, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${suggestion.id}`} color="link" size="sm">
                      {suggestion.id}
                    </Button>
                  </td>
                  <td>{suggestion.name}</td>
                  <td>{suggestion.ascii}</td>
                  <td>{suggestion.altName}</td>
                  <td>{suggestion.featClass}</td>
                  <td>{suggestion.featCode}</td>
                  <td>{suggestion.country}</td>
                  <td>{suggestion.cc2}</td>
                  <td>{suggestion.admin1}</td>
                  <td>{suggestion.admin2}</td>
                  <td>{suggestion.admin3}</td>
                  <td>{suggestion.admin4}</td>
                  <td>{suggestion.population}</td>
                  <td>{suggestion.elevation}</td>
                  <td>{suggestion.dem}</td>
                  <td>{suggestion.tz}</td>
                  <td>{suggestion.latitude}</td>
                  <td>{suggestion.longitude}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${suggestion.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${suggestion.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${suggestion.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Suggestions found</div>
        )}
      </div>
      <div className={suggestionList && suggestionList.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} />
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            activePage={paginationState.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={paginationState.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
    </div>
  );
};

const mapStateToProps = ({ suggestion }: IRootState) => ({
  suggestionList: suggestion.entities,
  loading: suggestion.loading,
  totalItems: suggestion.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Suggestion);
