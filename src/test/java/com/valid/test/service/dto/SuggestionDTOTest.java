package com.valid.test.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.valid.test.web.rest.TestUtil;

public class SuggestionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SuggestionDTO.class);
        SuggestionDTO suggestionDTO1 = new SuggestionDTO();
        suggestionDTO1.setId(1L);
        SuggestionDTO suggestionDTO2 = new SuggestionDTO();
        assertThat(suggestionDTO1).isNotEqualTo(suggestionDTO2);
        suggestionDTO2.setId(suggestionDTO1.getId());
        assertThat(suggestionDTO1).isEqualTo(suggestionDTO2);
        suggestionDTO2.setId(2L);
        assertThat(suggestionDTO1).isNotEqualTo(suggestionDTO2);
        suggestionDTO1.setId(null);
        assertThat(suggestionDTO1).isNotEqualTo(suggestionDTO2);
    }
}
