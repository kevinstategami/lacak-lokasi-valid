export interface ISuggestion {
  id?: number;
  name?: string;
  ascii?: string;
  altName?: string;
  featClass?: string;
  featCode?: string;
  country?: string;
  cc2?: string;
  admin1?: string;
  admin2?: string;
  admin3?: string;
  admin4?: string;
  population?: number;
  elevation?: number;
  dem?: number;
  tz?: string;
  latitude?: number;
  longitude?: number;
}

export const defaultValue: Readonly<ISuggestion> = {};
