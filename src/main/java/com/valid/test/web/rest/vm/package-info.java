/**
 * View Models used by Spring MVC REST controllers.
 */
package com.valid.test.web.rest.vm;
