package com.valid.test.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.valid.test.domain.Suggestion} entity.
 */
public class SuggestionDTO implements Serializable {

    private Long id;

    private String name;

    private String ascii;

    private String altName;

    private String featClass;

    private String featCode;

    private String country;

    private String cc2;

    private String admin1;

    private String admin2;

    private String admin3;

    private String admin4;

    private Long population;

    private Integer elevation;

    private Long dem;

    private String tz;

    private Double latitude;

    private Double longitude;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAscii() {
        return ascii;
    }

    public void setAscii(String ascii) {
        this.ascii = ascii;
    }

    public String getAltName() {
        return altName;
    }

    public void setAltName(String altName) {
        this.altName = altName;
    }

    public String getFeatClass() {
        return featClass;
    }

    public void setFeatClass(String featClass) {
        this.featClass = featClass;
    }

    public String getFeatCode() {
        return featCode;
    }

    public void setFeatCode(String featCode) {
        this.featCode = featCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCc2() {
        return cc2;
    }

    public void setCc2(String cc2) {
        this.cc2 = cc2;
    }

    public String getAdmin1() {
        return admin1;
    }

    public void setAdmin1(String admin1) {
        this.admin1 = admin1;
    }

    public String getAdmin2() {
        return admin2;
    }

    public void setAdmin2(String admin2) {
        this.admin2 = admin2;
    }

    public String getAdmin3() {
        return admin3;
    }

    public void setAdmin3(String admin3) {
        this.admin3 = admin3;
    }

    public String getAdmin4() {
        return admin4;
    }

    public void setAdmin4(String admin4) {
        this.admin4 = admin4;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public Long getDem() {
        return dem;
    }

    public void setDem(Long dem) {
        this.dem = dem;
    }

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SuggestionDTO suggestionDTO = (SuggestionDTO) o;
        if (suggestionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), suggestionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SuggestionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ascii='" + getAscii() + "'" +
            ", altName='" + getAltName() + "'" +
            ", featClass='" + getFeatClass() + "'" +
            ", featCode='" + getFeatCode() + "'" +
            ", country='" + getCountry() + "'" +
            ", cc2='" + getCc2() + "'" +
            ", admin1='" + getAdmin1() + "'" +
            ", admin2='" + getAdmin2() + "'" +
            ", admin3='" + getAdmin3() + "'" +
            ", admin4='" + getAdmin4() + "'" +
            ", population=" + getPopulation() +
            ", elevation=" + getElevation() +
            ", dem=" + getDem() +
            ", tz='" + getTz() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            "}";
    }
}
