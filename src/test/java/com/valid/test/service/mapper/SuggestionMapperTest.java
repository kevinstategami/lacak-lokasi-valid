package com.valid.test.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SuggestionMapperTest {

    private SuggestionMapper suggestionMapper;

    @BeforeEach
    public void setUp() {
        suggestionMapper = new SuggestionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(suggestionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(suggestionMapper.fromId(null)).isNull();
    }
}
