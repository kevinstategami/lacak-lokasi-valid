package com.valid.test.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Suggestion.
 */
@Entity
@Table(name = "suggestion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Suggestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "ascii")
    private String ascii;

    @Column(name = "alt_name")
    private String altName;

    @Column(name = "feat_class")
    private String featClass;

    @Column(name = "feat_code")
    private String featCode;

    @Column(name = "country")
    private String country;

    @Column(name = "cc_2")
    private String cc2;

    @Column(name = "admin_1")
    private String admin1;

    @Column(name = "admin_2")
    private String admin2;

    @Column(name = "admin_3")
    private String admin3;

    @Column(name = "admin_4")
    private String admin4;

    @Column(name = "population")
    private Long population;

    @Column(name = "elevation")
    private Integer elevation;

    @Column(name = "dem")
    private Long dem;

    @Column(name = "tz")
    private String tz;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Suggestion name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAscii() {
        return ascii;
    }

    public Suggestion ascii(String ascii) {
        this.ascii = ascii;
        return this;
    }

    public void setAscii(String ascii) {
        this.ascii = ascii;
    }

    public String getAltName() {
        return altName;
    }

    public Suggestion altName(String altName) {
        this.altName = altName;
        return this;
    }

    public void setAltName(String altName) {
        this.altName = altName;
    }

    public String getFeatClass() {
        return featClass;
    }

    public Suggestion featClass(String featClass) {
        this.featClass = featClass;
        return this;
    }

    public void setFeatClass(String featClass) {
        this.featClass = featClass;
    }

    public String getFeatCode() {
        return featCode;
    }

    public Suggestion featCode(String featCode) {
        this.featCode = featCode;
        return this;
    }

    public void setFeatCode(String featCode) {
        this.featCode = featCode;
    }

    public String getCountry() {
        return country;
    }

    public Suggestion country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCc2() {
        return cc2;
    }

    public Suggestion cc2(String cc2) {
        this.cc2 = cc2;
        return this;
    }

    public void setCc2(String cc2) {
        this.cc2 = cc2;
    }

    public String getAdmin1() {
        return admin1;
    }

    public Suggestion admin1(String admin1) {
        this.admin1 = admin1;
        return this;
    }

    public void setAdmin1(String admin1) {
        this.admin1 = admin1;
    }

    public String getAdmin2() {
        return admin2;
    }

    public Suggestion admin2(String admin2) {
        this.admin2 = admin2;
        return this;
    }

    public void setAdmin2(String admin2) {
        this.admin2 = admin2;
    }

    public String getAdmin3() {
        return admin3;
    }

    public Suggestion admin3(String admin3) {
        this.admin3 = admin3;
        return this;
    }

    public void setAdmin3(String admin3) {
        this.admin3 = admin3;
    }

    public String getAdmin4() {
        return admin4;
    }

    public Suggestion admin4(String admin4) {
        this.admin4 = admin4;
        return this;
    }

    public void setAdmin4(String admin4) {
        this.admin4 = admin4;
    }

    public Long getPopulation() {
        return population;
    }

    public Suggestion population(Long population) {
        this.population = population;
        return this;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public Integer getElevation() {
        return elevation;
    }

    public Suggestion elevation(Integer elevation) {
        this.elevation = elevation;
        return this;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public Long getDem() {
        return dem;
    }

    public Suggestion dem(Long dem) {
        this.dem = dem;
        return this;
    }

    public void setDem(Long dem) {
        this.dem = dem;
    }

    public String getTz() {
        return tz;
    }

    public Suggestion tz(String tz) {
        this.tz = tz;
        return this;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Suggestion latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Suggestion longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Suggestion)) {
            return false;
        }
        return id != null && id.equals(((Suggestion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Suggestion{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ascii='" + getAscii() + "'" +
            ", altName='" + getAltName() + "'" +
            ", featClass='" + getFeatClass() + "'" +
            ", featCode='" + getFeatCode() + "'" +
            ", country='" + getCountry() + "'" +
            ", cc2='" + getCc2() + "'" +
            ", admin1='" + getAdmin1() + "'" +
            ", admin2='" + getAdmin2() + "'" +
            ", admin3='" + getAdmin3() + "'" +
            ", admin4='" + getAdmin4() + "'" +
            ", population=" + getPopulation() +
            ", elevation=" + getElevation() +
            ", dem=" + getDem() +
            ", tz='" + getTz() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            "}";
    }
}
