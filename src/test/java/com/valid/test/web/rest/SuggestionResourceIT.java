package com.valid.test.web.rest;

import com.valid.test.ValidTestApp;
import com.valid.test.domain.Suggestion;
import com.valid.test.repository.SuggestionRepository;
import com.valid.test.service.SuggestionService;
import com.valid.test.service.dto.SuggestionDTO;
import com.valid.test.service.mapper.SuggestionMapper;
import com.valid.test.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.valid.test.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SuggestionResource} REST controller.
 */
@SpringBootTest(classes = ValidTestApp.class)
public class SuggestionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ASCII = "AAAAAAAAAA";
    private static final String UPDATED_ASCII = "BBBBBBBBBB";

    private static final String DEFAULT_ALT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ALT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FEAT_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_FEAT_CLASS = "BBBBBBBBBB";

    private static final String DEFAULT_FEAT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_FEAT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_CC_2 = "AAAAAAAAAA";
    private static final String UPDATED_CC_2 = "BBBBBBBBBB";

    private static final String DEFAULT_ADMIN_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADMIN_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ADMIN_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADMIN_2 = "BBBBBBBBBB";

    private static final String DEFAULT_ADMIN_3 = "AAAAAAAAAA";
    private static final String UPDATED_ADMIN_3 = "BBBBBBBBBB";

    private static final String DEFAULT_ADMIN_4 = "AAAAAAAAAA";
    private static final String UPDATED_ADMIN_4 = "BBBBBBBBBB";

    private static final Long DEFAULT_POPULATION = 1L;
    private static final Long UPDATED_POPULATION = 2L;

    private static final Integer DEFAULT_ELEVATION = 1;
    private static final Integer UPDATED_ELEVATION = 2;

    private static final Long DEFAULT_DEM = 1L;
    private static final Long UPDATED_DEM = 2L;

    private static final String DEFAULT_TZ = "AAAAAAAAAA";
    private static final String UPDATED_TZ = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    @Autowired
    private SuggestionRepository suggestionRepository;

    @Autowired
    private SuggestionMapper suggestionMapper;

    @Autowired
    private SuggestionService suggestionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSuggestionMockMvc;

    private Suggestion suggestion;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SuggestionResource suggestionResource = new SuggestionResource(suggestionService);
        this.restSuggestionMockMvc = MockMvcBuilders.standaloneSetup(suggestionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Suggestion createEntity(EntityManager em) {
        Suggestion suggestion = new Suggestion()
            .name(DEFAULT_NAME)
            .ascii(DEFAULT_ASCII)
            .altName(DEFAULT_ALT_NAME)
            .featClass(DEFAULT_FEAT_CLASS)
            .featCode(DEFAULT_FEAT_CODE)
            .country(DEFAULT_COUNTRY)
            .cc2(DEFAULT_CC_2)
            .admin1(DEFAULT_ADMIN_1)
            .admin2(DEFAULT_ADMIN_2)
            .admin3(DEFAULT_ADMIN_3)
            .admin4(DEFAULT_ADMIN_4)
            .population(DEFAULT_POPULATION)
            .elevation(DEFAULT_ELEVATION)
            .dem(DEFAULT_DEM)
            .tz(DEFAULT_TZ)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE);
        return suggestion;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Suggestion createUpdatedEntity(EntityManager em) {
        Suggestion suggestion = new Suggestion()
            .name(UPDATED_NAME)
            .ascii(UPDATED_ASCII)
            .altName(UPDATED_ALT_NAME)
            .featClass(UPDATED_FEAT_CLASS)
            .featCode(UPDATED_FEAT_CODE)
            .country(UPDATED_COUNTRY)
            .cc2(UPDATED_CC_2)
            .admin1(UPDATED_ADMIN_1)
            .admin2(UPDATED_ADMIN_2)
            .admin3(UPDATED_ADMIN_3)
            .admin4(UPDATED_ADMIN_4)
            .population(UPDATED_POPULATION)
            .elevation(UPDATED_ELEVATION)
            .dem(UPDATED_DEM)
            .tz(UPDATED_TZ)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE);
        return suggestion;
    }

    @BeforeEach
    public void initTest() {
        suggestion = createEntity(em);
    }

    @Test
    @Transactional
    public void createSuggestion() throws Exception {
        int databaseSizeBeforeCreate = suggestionRepository.findAll().size();

        // Create the Suggestion
        SuggestionDTO suggestionDTO = suggestionMapper.toDto(suggestion);
        restSuggestionMockMvc.perform(post("/api/suggestions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(suggestionDTO)))
            .andExpect(status().isCreated());

        // Validate the Suggestion in the database
        List<Suggestion> suggestionList = suggestionRepository.findAll();
        assertThat(suggestionList).hasSize(databaseSizeBeforeCreate + 1);
        Suggestion testSuggestion = suggestionList.get(suggestionList.size() - 1);
        assertThat(testSuggestion.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSuggestion.getAscii()).isEqualTo(DEFAULT_ASCII);
        assertThat(testSuggestion.getAltName()).isEqualTo(DEFAULT_ALT_NAME);
        assertThat(testSuggestion.getFeatClass()).isEqualTo(DEFAULT_FEAT_CLASS);
        assertThat(testSuggestion.getFeatCode()).isEqualTo(DEFAULT_FEAT_CODE);
        assertThat(testSuggestion.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testSuggestion.getCc2()).isEqualTo(DEFAULT_CC_2);
        assertThat(testSuggestion.getAdmin1()).isEqualTo(DEFAULT_ADMIN_1);
        assertThat(testSuggestion.getAdmin2()).isEqualTo(DEFAULT_ADMIN_2);
        assertThat(testSuggestion.getAdmin3()).isEqualTo(DEFAULT_ADMIN_3);
        assertThat(testSuggestion.getAdmin4()).isEqualTo(DEFAULT_ADMIN_4);
        assertThat(testSuggestion.getPopulation()).isEqualTo(DEFAULT_POPULATION);
        assertThat(testSuggestion.getElevation()).isEqualTo(DEFAULT_ELEVATION);
        assertThat(testSuggestion.getDem()).isEqualTo(DEFAULT_DEM);
        assertThat(testSuggestion.getTz()).isEqualTo(DEFAULT_TZ);
        assertThat(testSuggestion.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testSuggestion.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
    }

    @Test
    @Transactional
    public void createSuggestionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = suggestionRepository.findAll().size();

        // Create the Suggestion with an existing ID
        suggestion.setId(1L);
        SuggestionDTO suggestionDTO = suggestionMapper.toDto(suggestion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSuggestionMockMvc.perform(post("/api/suggestions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(suggestionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Suggestion in the database
        List<Suggestion> suggestionList = suggestionRepository.findAll();
        assertThat(suggestionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSuggestions() throws Exception {
        // Initialize the database
        suggestionRepository.saveAndFlush(suggestion);

        // Get all the suggestionList
        restSuggestionMockMvc.perform(get("/api/suggestions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(suggestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].ascii").value(hasItem(DEFAULT_ASCII)))
            .andExpect(jsonPath("$.[*].altName").value(hasItem(DEFAULT_ALT_NAME)))
            .andExpect(jsonPath("$.[*].featClass").value(hasItem(DEFAULT_FEAT_CLASS)))
            .andExpect(jsonPath("$.[*].featCode").value(hasItem(DEFAULT_FEAT_CODE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].cc2").value(hasItem(DEFAULT_CC_2)))
            .andExpect(jsonPath("$.[*].admin1").value(hasItem(DEFAULT_ADMIN_1)))
            .andExpect(jsonPath("$.[*].admin2").value(hasItem(DEFAULT_ADMIN_2)))
            .andExpect(jsonPath("$.[*].admin3").value(hasItem(DEFAULT_ADMIN_3)))
            .andExpect(jsonPath("$.[*].admin4").value(hasItem(DEFAULT_ADMIN_4)))
            .andExpect(jsonPath("$.[*].population").value(hasItem(DEFAULT_POPULATION.intValue())))
            .andExpect(jsonPath("$.[*].elevation").value(hasItem(DEFAULT_ELEVATION)))
            .andExpect(jsonPath("$.[*].dem").value(hasItem(DEFAULT_DEM.intValue())))
            .andExpect(jsonPath("$.[*].tz").value(hasItem(DEFAULT_TZ)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getSuggestion() throws Exception {
        // Initialize the database
        suggestionRepository.saveAndFlush(suggestion);

        // Get the suggestion
        restSuggestionMockMvc.perform(get("/api/suggestions/{id}", suggestion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(suggestion.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.ascii").value(DEFAULT_ASCII))
            .andExpect(jsonPath("$.altName").value(DEFAULT_ALT_NAME))
            .andExpect(jsonPath("$.featClass").value(DEFAULT_FEAT_CLASS))
            .andExpect(jsonPath("$.featCode").value(DEFAULT_FEAT_CODE))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.cc2").value(DEFAULT_CC_2))
            .andExpect(jsonPath("$.admin1").value(DEFAULT_ADMIN_1))
            .andExpect(jsonPath("$.admin2").value(DEFAULT_ADMIN_2))
            .andExpect(jsonPath("$.admin3").value(DEFAULT_ADMIN_3))
            .andExpect(jsonPath("$.admin4").value(DEFAULT_ADMIN_4))
            .andExpect(jsonPath("$.population").value(DEFAULT_POPULATION.intValue()))
            .andExpect(jsonPath("$.elevation").value(DEFAULT_ELEVATION))
            .andExpect(jsonPath("$.dem").value(DEFAULT_DEM.intValue()))
            .andExpect(jsonPath("$.tz").value(DEFAULT_TZ))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSuggestion() throws Exception {
        // Get the suggestion
        restSuggestionMockMvc.perform(get("/api/suggestions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSuggestion() throws Exception {
        // Initialize the database
        suggestionRepository.saveAndFlush(suggestion);

        int databaseSizeBeforeUpdate = suggestionRepository.findAll().size();

        // Update the suggestion
        Suggestion updatedSuggestion = suggestionRepository.findById(suggestion.getId()).get();
        // Disconnect from session so that the updates on updatedSuggestion are not directly saved in db
        em.detach(updatedSuggestion);
        updatedSuggestion
            .name(UPDATED_NAME)
            .ascii(UPDATED_ASCII)
            .altName(UPDATED_ALT_NAME)
            .featClass(UPDATED_FEAT_CLASS)
            .featCode(UPDATED_FEAT_CODE)
            .country(UPDATED_COUNTRY)
            .cc2(UPDATED_CC_2)
            .admin1(UPDATED_ADMIN_1)
            .admin2(UPDATED_ADMIN_2)
            .admin3(UPDATED_ADMIN_3)
            .admin4(UPDATED_ADMIN_4)
            .population(UPDATED_POPULATION)
            .elevation(UPDATED_ELEVATION)
            .dem(UPDATED_DEM)
            .tz(UPDATED_TZ)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE);
        SuggestionDTO suggestionDTO = suggestionMapper.toDto(updatedSuggestion);

        restSuggestionMockMvc.perform(put("/api/suggestions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(suggestionDTO)))
            .andExpect(status().isOk());

        // Validate the Suggestion in the database
        List<Suggestion> suggestionList = suggestionRepository.findAll();
        assertThat(suggestionList).hasSize(databaseSizeBeforeUpdate);
        Suggestion testSuggestion = suggestionList.get(suggestionList.size() - 1);
        assertThat(testSuggestion.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSuggestion.getAscii()).isEqualTo(UPDATED_ASCII);
        assertThat(testSuggestion.getAltName()).isEqualTo(UPDATED_ALT_NAME);
        assertThat(testSuggestion.getFeatClass()).isEqualTo(UPDATED_FEAT_CLASS);
        assertThat(testSuggestion.getFeatCode()).isEqualTo(UPDATED_FEAT_CODE);
        assertThat(testSuggestion.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testSuggestion.getCc2()).isEqualTo(UPDATED_CC_2);
        assertThat(testSuggestion.getAdmin1()).isEqualTo(UPDATED_ADMIN_1);
        assertThat(testSuggestion.getAdmin2()).isEqualTo(UPDATED_ADMIN_2);
        assertThat(testSuggestion.getAdmin3()).isEqualTo(UPDATED_ADMIN_3);
        assertThat(testSuggestion.getAdmin4()).isEqualTo(UPDATED_ADMIN_4);
        assertThat(testSuggestion.getPopulation()).isEqualTo(UPDATED_POPULATION);
        assertThat(testSuggestion.getElevation()).isEqualTo(UPDATED_ELEVATION);
        assertThat(testSuggestion.getDem()).isEqualTo(UPDATED_DEM);
        assertThat(testSuggestion.getTz()).isEqualTo(UPDATED_TZ);
        assertThat(testSuggestion.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testSuggestion.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void updateNonExistingSuggestion() throws Exception {
        int databaseSizeBeforeUpdate = suggestionRepository.findAll().size();

        // Create the Suggestion
        SuggestionDTO suggestionDTO = suggestionMapper.toDto(suggestion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSuggestionMockMvc.perform(put("/api/suggestions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(suggestionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Suggestion in the database
        List<Suggestion> suggestionList = suggestionRepository.findAll();
        assertThat(suggestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSuggestion() throws Exception {
        // Initialize the database
        suggestionRepository.saveAndFlush(suggestion);

        int databaseSizeBeforeDelete = suggestionRepository.findAll().size();

        // Delete the suggestion
        restSuggestionMockMvc.perform(delete("/api/suggestions/{id}", suggestion.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Suggestion> suggestionList = suggestionRepository.findAll();
        assertThat(suggestionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
