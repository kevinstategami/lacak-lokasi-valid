package com.valid.test.Interface;

import java.math.BigDecimal;

public interface SuggestionInterface {

    String getName();
    BigDecimal getLongitude();
    BigDecimal getLatitude();
    BigDecimal getScore();

}
