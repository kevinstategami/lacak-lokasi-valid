package com.valid.test.repository;

import com.valid.test.Interface.SuggestionInterface;
import com.valid.test.domain.Suggestion;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Spring Data  repository for the Suggestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SuggestionRepository extends JpaRepository<Suggestion, Long> {
    @Query(value="select sg.name as name, sg.longitude as longitude, sg.latitude as latitude,  " +
        "ROUND((func_distances(point(sg.longitude,sg.latitude), point( :longitude, :latitude)) / 10000),2) as score," +
        "func_distances(point(sg.longitude,sg.latitude), point( :longitude, :latitude)) as distance " +
        "from suggestions sg " +
        "where sg.name like :spaceName% and func_distances(point(sg.longitude,sg.latitude), point( :longitude,:latitude)) " +
        "order by distance asc", nativeQuery = true)
    List<SuggestionInterface> getByNameAndLocationNear(@Param("spaceName") String spaceName, @Param("longitude") BigDecimal longitude, @Param("latitude") BigDecimal latitude);

}
