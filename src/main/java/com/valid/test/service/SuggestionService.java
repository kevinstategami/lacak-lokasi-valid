package com.valid.test.service;

import com.valid.test.Interface.SuggestionInterface;
import com.valid.test.service.dto.SuggestionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.valid.test.domain.Suggestion}.
 */
public interface SuggestionService {

    /**
     * Save a suggestion.
     *
     * @param suggestionDTO the entity to save.
     * @return the persisted entity.
     */
    SuggestionDTO save(SuggestionDTO suggestionDTO);

    /**
     * Get all the suggestions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SuggestionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" suggestion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SuggestionDTO> findOne(Long id);

    /**
     * Delete the "id" suggestion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<SuggestionInterface> getByNameAndLocationNear(String name, BigDecimal latitude, BigDecimal longitude) throws Exception;
}
