package com.valid.test.web.rest;

import com.valid.test.Interface.SuggestionInterface;
import com.valid.test.service.SuggestionService;
import com.valid.test.web.rest.errors.BadRequestAlertException;
import com.valid.test.service.dto.SuggestionDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.valid.test.domain.Suggestion}.
 */
@RestController
@RequestMapping("/api")
public class SuggestionResource {

    private final Logger log = LoggerFactory.getLogger(SuggestionResource.class);

    private static final String ENTITY_NAME = "suggestion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SuggestionService suggestionService;

    public SuggestionResource(SuggestionService suggestionService) {
        this.suggestionService = suggestionService;
    }

    /**
     * {@code POST  /suggestions} : Create a new suggestion.
     *
     * @param suggestionDTO the suggestionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new suggestionDTO, or with status {@code 400 (Bad Request)} if the suggestion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/suggestions")
    public ResponseEntity<SuggestionDTO> createSuggestion(@RequestBody SuggestionDTO suggestionDTO) throws URISyntaxException {
        log.debug("REST request to save Suggestion : {}", suggestionDTO);
        if (suggestionDTO.getId() != null) {
            throw new BadRequestAlertException("A new suggestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SuggestionDTO result = suggestionService.save(suggestionDTO);
        return ResponseEntity.created(new URI("/api/suggestions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /suggestions} : Updates an existing suggestion.
     *
     * @param suggestionDTO the suggestionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated suggestionDTO,
     * or with status {@code 400 (Bad Request)} if the suggestionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the suggestionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/suggestions")
    public ResponseEntity<SuggestionDTO> updateSuggestion(@RequestBody SuggestionDTO suggestionDTO) throws URISyntaxException {
        log.debug("REST request to update Suggestion : {}", suggestionDTO);
        if (suggestionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SuggestionDTO result = suggestionService.save(suggestionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, suggestionDTO.getId().toString()))
            .body(result);
    }


    /**
     * {@code GET  /suggestions/:id} : get the "id" suggestion.
     *
     * @param id the id of the suggestionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the suggestionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/suggestions/{id}")
    public ResponseEntity<SuggestionDTO> getSuggestion(@PathVariable Long id) {
        log.debug("REST request to get Suggestion : {}", id);
        Optional<SuggestionDTO> suggestionDTO = suggestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(suggestionDTO);
    }

    /**
     * {@code DELETE  /suggestions/:id} : delete the "id" suggestion.
     *
     * @param id the id of the suggestionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/suggestions/{id}")
    public ResponseEntity<Void> deleteSuggestion(@PathVariable Long id) {
        log.debug("REST request to delete Suggestion : {}", id);
        suggestionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/suggestions")
    public ResponseEntity<List<SuggestionInterface>> getByNameAndLocationNear(@RequestParam String name, @RequestParam BigDecimal latitude, @RequestParam BigDecimal longitude) throws Exception {
        log.debug("REST request to get a page of Suggestions by name, lat, long : {},{},{}",name,latitude,longitude);
        List<SuggestionInterface> entityList = suggestionService.getByNameAndLocationNear(name,latitude,longitude);
        return ResponseEntity.ok().body(entityList);
    }

}
