package com.valid.test.service.mapper;


import com.valid.test.domain.*;
import com.valid.test.service.dto.SuggestionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Suggestion} and its DTO {@link SuggestionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SuggestionMapper extends EntityMapper<SuggestionDTO, Suggestion> {



    default Suggestion fromId(Long id) {
        if (id == null) {
            return null;
        }
        Suggestion suggestion = new Suggestion();
        suggestion.setId(id);
        return suggestion;
    }
}
