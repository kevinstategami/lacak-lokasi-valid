import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './suggestion.reducer';
import { ISuggestion } from 'app/shared/model/suggestion.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISuggestionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SuggestionUpdate = (props: ISuggestionUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { suggestionEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/suggestion' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...suggestionEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="validTestApp.suggestion.home.createOrEditLabel">Create or edit a Suggestion</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : suggestionEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="suggestion-id">ID</Label>
                  <AvInput id="suggestion-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="suggestion-name">
                  Name
                </Label>
                <AvField id="suggestion-name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="asciiLabel" for="suggestion-ascii">
                  Ascii
                </Label>
                <AvField id="suggestion-ascii" type="text" name="ascii" />
              </AvGroup>
              <AvGroup>
                <Label id="altNameLabel" for="suggestion-altName">
                  Alt Name
                </Label>
                <AvField id="suggestion-altName" type="text" name="altName" />
              </AvGroup>
              <AvGroup>
                <Label id="featClassLabel" for="suggestion-featClass">
                  Feat Class
                </Label>
                <AvField id="suggestion-featClass" type="text" name="featClass" />
              </AvGroup>
              <AvGroup>
                <Label id="featCodeLabel" for="suggestion-featCode">
                  Feat Code
                </Label>
                <AvField id="suggestion-featCode" type="text" name="featCode" />
              </AvGroup>
              <AvGroup>
                <Label id="countryLabel" for="suggestion-country">
                  Country
                </Label>
                <AvField id="suggestion-country" type="text" name="country" />
              </AvGroup>
              <AvGroup>
                <Label id="cc2Label" for="suggestion-cc2">
                  Cc 2
                </Label>
                <AvField id="suggestion-cc2" type="text" name="cc2" />
              </AvGroup>
              <AvGroup>
                <Label id="admin1Label" for="suggestion-admin1">
                  Admin 1
                </Label>
                <AvField id="suggestion-admin1" type="text" name="admin1" />
              </AvGroup>
              <AvGroup>
                <Label id="admin2Label" for="suggestion-admin2">
                  Admin 2
                </Label>
                <AvField id="suggestion-admin2" type="text" name="admin2" />
              </AvGroup>
              <AvGroup>
                <Label id="admin3Label" for="suggestion-admin3">
                  Admin 3
                </Label>
                <AvField id="suggestion-admin3" type="text" name="admin3" />
              </AvGroup>
              <AvGroup>
                <Label id="admin4Label" for="suggestion-admin4">
                  Admin 4
                </Label>
                <AvField id="suggestion-admin4" type="text" name="admin4" />
              </AvGroup>
              <AvGroup>
                <Label id="populationLabel" for="suggestion-population">
                  Population
                </Label>
                <AvField id="suggestion-population" type="string" className="form-control" name="population" />
              </AvGroup>
              <AvGroup>
                <Label id="elevationLabel" for="suggestion-elevation">
                  Elevation
                </Label>
                <AvField id="suggestion-elevation" type="string" className="form-control" name="elevation" />
              </AvGroup>
              <AvGroup>
                <Label id="demLabel" for="suggestion-dem">
                  Dem
                </Label>
                <AvField id="suggestion-dem" type="string" className="form-control" name="dem" />
              </AvGroup>
              <AvGroup>
                <Label id="tzLabel" for="suggestion-tz">
                  Tz
                </Label>
                <AvField id="suggestion-tz" type="text" name="tz" />
              </AvGroup>
              <AvGroup>
                <Label id="latitudeLabel" for="suggestion-latitude">
                  Latitude
                </Label>
                <AvField id="suggestion-latitude" type="string" className="form-control" name="latitude" />
              </AvGroup>
              <AvGroup>
                <Label id="longitudeLabel" for="suggestion-longitude">
                  Longitude
                </Label>
                <AvField id="suggestion-longitude" type="string" className="form-control" name="longitude" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/suggestion" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  suggestionEntity: storeState.suggestion.entity,
  loading: storeState.suggestion.loading,
  updating: storeState.suggestion.updating,
  updateSuccess: storeState.suggestion.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SuggestionUpdate);
