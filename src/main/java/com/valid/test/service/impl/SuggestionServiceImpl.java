package com.valid.test.service.impl;

import com.valid.test.Interface.SuggestionInterface;
import com.valid.test.service.SuggestionService;
import com.valid.test.domain.Suggestion;
import com.valid.test.repository.SuggestionRepository;
import com.valid.test.service.dto.SuggestionDTO;
import com.valid.test.service.mapper.SuggestionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Suggestion}.
 */
@Service
@Transactional
public class SuggestionServiceImpl implements SuggestionService {

    private final Logger log = LoggerFactory.getLogger(SuggestionServiceImpl.class);

    private final SuggestionRepository suggestionRepository;

    private final SuggestionMapper suggestionMapper;

    public SuggestionServiceImpl(SuggestionRepository suggestionRepository, SuggestionMapper suggestionMapper) {
        this.suggestionRepository = suggestionRepository;
        this.suggestionMapper = suggestionMapper;
    }

    /**
     * Save a suggestion.
     *
     * @param suggestionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SuggestionDTO save(SuggestionDTO suggestionDTO) {
        log.debug("Request to save Suggestion : {}", suggestionDTO);
        Suggestion suggestion = suggestionMapper.toEntity(suggestionDTO);
        suggestion = suggestionRepository.save(suggestion);
        return suggestionMapper.toDto(suggestion);
    }

    /**
     * Get all the suggestions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SuggestionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Suggestions");
        return suggestionRepository.findAll(pageable)
            .map(suggestionMapper::toDto);
    }

    /**
     * Get one suggestion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SuggestionDTO> findOne(Long id) {
        log.debug("Request to get Suggestion : {}", id);
        return suggestionRepository.findById(id)
            .map(suggestionMapper::toDto);
    }

    /**
     * Delete the suggestion by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Suggestion : {}", id);
        suggestionRepository.deleteById(id);
    }
    @Override
    public List<SuggestionInterface> getByNameAndLocationNear(String name, BigDecimal latitude, BigDecimal longitude) throws Exception {
        log.debug("REST request to get a page of Suggestions by name, lat, long : {},{},{}",name,latitude,longitude);
        System.out.println(">>> "+ name + " " + longitude + " " +latitude);
        return suggestionRepository.getByNameAndLocationNear(name,longitude,latitude);
    }
}
